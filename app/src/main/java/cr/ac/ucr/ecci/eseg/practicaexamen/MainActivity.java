package cr.ac.ucr.ecci.eseg.practicaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.eliminarElementos();
        this.insertarElementos();
        setContentView(R.layout.activity_main);

    }

    private void insertarElementos(){
        for(TableTop juego : Juegos.juegos){
            TableTop.insertar(getBaseContext(), juego);
        }
    }

    private void eliminarElementos(){
        TableTop.eliminarDatos(getBaseContext());
    }
}