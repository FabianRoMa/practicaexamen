package cr.ac.ucr.ecci.eseg.practicaexamen;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import cr.ac.ucr.ecci.eseg.practicaexamen.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyTableTopRecyclerViewAdapter extends RecyclerView.Adapter<MyTableTopRecyclerViewAdapter.ViewHolder> {

    private final List<TableTop> mValues;

    public MyTableTopRecyclerViewAdapter(List<TableTop> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        //holder.mIdView.setText(mValues.get(position).id);
        //holder.mContentView.setText(mValues.get(position).content);
        holder.nombreText.setText(mValues.get(position).name);
        holder.descriptionText.setText(mValues.get(position).descripcion);
        holder.iconoImage.setImageResource(elegirIcono(mValues.get(position).ID));
    }
    private int  elegirIcono(String id){
        switch (id){
            case "TT001":
                return R.drawable.catan;
            case "TT002":
                return R.drawable.monopoly;
            case "TT003":
                return R.drawable.eldritch;
            case "TT004":
                return R.drawable.mtg;
            case "TT005":
                return R.drawable.hanabi;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        //public final TextView mIdView;
        //public final TextView mContentView;

        public final TextView nombreText;
        public final TextView descriptionText;
        public final ImageView iconoImage;
        public TableTop mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            //mIdView = (TextView) view.findViewById(R.id.item_number);
            //mContentView = (TextView) view.findViewById(R.id.content);
            nombreText = (TextView) view.findViewById(R.id.nombreText);
            descriptionText = (TextView) view.findViewById(R.id.descripcion);
            iconoImage = (ImageView) view.findViewById(R.id.icono);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mView.getContext(), DetailsTableActivity.class);
                    intent.putExtra("tableTop", mItem);
                    mView.getContext().startActivity(intent);
                }
            });
        }


        @Override
        public String toString() {
            return super.toString() + " '" + descriptionText.getText() + "'";
        }
    }
}