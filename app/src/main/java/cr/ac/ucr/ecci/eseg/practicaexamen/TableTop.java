package cr.ac.ucr.ecci.eseg.practicaexamen;

import android.app.ActionBar;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class TableTop implements Parcelable {
    String ID;
    String name;
    String year;
    String publisher;
    String country;
    String latitude;
    String longitude;
    String descripcion;
    String noPlayer;
    String ages;
    String playingTime;

    public TableTop(){ }

    public TableTop(String ID, String name, String year, String publisher, String country, String latitude, String longitude, String descripcion, String noPlayer, String ages, String playingTime) {
        this.ID = ID;
        this.name = name;
        this.year = year;
        this.publisher = publisher;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.descripcion = descripcion;
        this.noPlayer = noPlayer;
        this.ages = ages;
        this.playingTime = playingTime;
    }

    protected TableTop(Parcel in){
        this.ID = in.readString();
        this.name = in.readString();
        this.year = in.readString();
        this.publisher = in.readString();
        this.country = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.descripcion = in.readString();
        this.noPlayer = in.readString();
        this.ages = in.readString();
        this.playingTime = in.readString();
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ID);
        parcel.writeString(name);
        parcel.writeString(year);
        parcel.writeString(publisher);
        parcel.writeString(country);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(descripcion);
        parcel.writeString(noPlayer);
        parcel.writeString(ages);
        parcel.writeString(playingTime);
    }

    public static long insertar(Context context, TableTop tableTop){
        // usar la clase DataBaseHelper para realizar la operacion de insertar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        // Crear un mapa de valores donde las columnas son las llav
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_ID, tableTop.ID);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME, tableTop.name);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR, tableTop.year);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER, tableTop.publisher);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY, tableTop.country);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE, tableTop.latitude);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE, tableTop.longitude);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION, tableTop.descripcion);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NO_PLAYER, tableTop.noPlayer);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES, tableTop.ages);
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYING_TIME, tableTop.playingTime);
        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, null, values);
    }

    public static List<TableTop> leer(Context context){
        List<TableTop> resultados = new ArrayList<>();
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String[] projection = {
                DataBaseContract.DataBaseEntry.COLUMN_NAME_ID,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NO_PLAYER,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYING_TIME
        };
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP,
                projection,
                null,
                null,
                null,
                null,
                null,
                null
        );
        while (cursor.moveToNext() && cursor.getCount() > 0) {
            TableTop punto = new TableTop(
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NO_PLAYER)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYING_TIME))
            );
            resultados.add(punto);
        }
        return resultados;
    }
    public static void  eliminarDatos(Context context){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        //Obtenemos una instancia en modo escritura de la base de datos.
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP,null ,null );
    }
}
