package cr.ac.ucr.ecci.eseg.practicaexamen;

import android.app.ActionBar;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsTableFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsTableFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TABLETOP = "tableTop";
    // TODO: Rename and change types of parameters
    private TableTop tableTop;

    public DetailsTableFragment() {
        // Required empty public constructor
    }

    public DetailsTableFragment(TableTop tableTop) {
        // Required empty public constructor
        this.tableTop = tableTop;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsTableFragment newInstance(TableTop tableTop) {
        DetailsTableFragment fragment = new DetailsTableFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_TABLETOP, tableTop);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tableTop = getArguments().getParcelable(ARG_TABLETOP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_details_table, container, false);
        TextView nameText = (TextView) v.findViewById(R.id.nameText);
        TextView yeartText = (TextView) v.findViewById(R.id.yearText);
        TextView publisherText = (TextView) v.findViewById(R.id.publisherText);
        TextView playinTimeText = (TextView) v.findViewById(R.id.playingTimeText);
        TextView countryText = (TextView) v.findViewById(R.id.countryText);
        TextView ubicacionText = (TextView) v.findViewById(R.id.ubicacionText);

        nameText.setText("Nombre: " + tableTop.name);
        yeartText.setText("Año de creacion: " + tableTop.year);
        publisherText.setText("Publisher: " + tableTop.publisher);
        playinTimeText.setText(" Playing time: " + tableTop.playingTime);
        countryText.setText("Pais: " + tableTop.country);
        ubicacionText.setText("Ubicacion en el mapa");
        ubicacionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Me dio pereza sowwy", Toast.LENGTH_SHORT).show();
            }
        });


        return v;
    }
}