package cr.ac.ucr.ecci.eseg.practicaexamen;

import android.provider.BaseColumns;

public class DataBaseContract {

    private DataBaseContract(){}

    public static class DataBaseEntry implements BaseColumns{
        //
        public static final String TABLE_NAME_TABLETOP = "TableTop";
        //
        public static final String COLUMN_NAME_ID = "ID";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_YEAR = "year";
        public static final String COLUMN_NAME_PUBLISHER = "publisher";
        public static final String COLUMN_NAME_COUNTRY = "country";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_DESCRIPTION = "descripcion";
        public static final String COLUMN_NAME_NO_PLAYER = "noPlayer";
        public static final String COLUMN_NAME_AGES = "ages";
        public static final String COLUMN_NAME_PLAYING_TIME = "playingTime";
    }
    //Mapeo de tipos
    private static final String TEXT_TYPE = " TEXT ";
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_TABLE_TOP =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP + " (" +
                    DataBaseEntry.COLUMN_NAME_ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_YEAR + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LATITUDE + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LONGITUDE + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NO_PLAYER + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PLAYING_TIME + TEXT_TYPE + " )";

    public static final String SQL_DELETE_TABLE_TOP =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;

}
